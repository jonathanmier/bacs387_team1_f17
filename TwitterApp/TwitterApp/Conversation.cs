﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi.Models;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace TwitterApp
{
    /*
     * MS: This class is good... nice way 
     * of extracting some of the behaviors around
     * PM threads and keeping the PM class clean.
     */
    class Conversation : PM
    {
        public Participants Participants { get; set; }
        public List<IMessage> MessagesRecieved { get; set; }
        public List<IMessage> MessagesSent { get; set; }
        public List<IMessage> sortMessages()
        {
            List<IMessage> SortedMessages = new List<IMessage>();
            foreach (IMessage Message in MessagesRecieved)
            {
                if (Message.SenderScreenName.ToString().ToUpper().Equals(Participants.Recipiant.ToUpper()))
                {
                    SortedMessages.Add(Message);
                }
            }
            foreach (IMessage Message in MessagesSent)
            {
                if (Message.RecipientScreenName.ToString().ToUpper().Equals(Participants.Recipiant.ToUpper()))
                {
                    SortedMessages.Add(Message);
                }
            }
            return SortedMessages;
        }
        //Saves messages to textfile
        public void SaveMessages()
        {
            // I could probaly use something better than a try/catch but it works.
            try
            { 
                //creates a file on desktop
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string path = desktopPath + "\\MessageHistoryWith_" + Participants.Recipiant + ".txt";
            string PrivateMessage;
                //specifies the streamwriter to append
            StreamWriter sw = new StreamWriter(path, append: true);
           //Records the time messages was created, sender and message text
                foreach (IMessage message in sortMessages().OrderBy(d => d.CreatedAt))
                {
                    if (message.SenderScreenName.Equals(Participants.Sender))
                    {
                        PrivateMessage = "Sent at " + message.CreatedAt + ": (" + message.RecipientScreenName + ") " + message.Text;
                    }
                    else
                    {
                        PrivateMessage = "Sent at " + message.CreatedAt + ": (" + message.SenderScreenName + ") " + message.Text;
                    }
                    sw.WriteLine(PrivateMessage);
                }

                sw.Close();
                //A notificaton so the user knows the action has been performed.
                MessageBox.Show("This conversation has been saved to your desktop. \nThe conversation is saved as:" + "\nMessageHistoryWith_" +
                    Participants.Recipiant, "Message Saved");
            }
            catch
            {
                //An error message if no conversation is selected
                MessageBox.Show("Please choose a conversation to save.", "No Conversation Selected",MessageBoxButton.OK,MessageBoxImage.Warning);
            }
            }
        
          
        }


    }


    

