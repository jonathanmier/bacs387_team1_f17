﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;

namespace TwitterApp
{
    class PM : UncMessage
    { 
    
        public PM()
        {
            CharacterLimit = 10000;
        }

        /*
         * MS: I know it's overkill, but I would like to see
         * this data encapsulated in an object / objects here.
         * Either set it as properties on this object or
         * create a data only object to send in as a parameter here. 
         * Avoid sending strings / raw data ... I want you to 
         * always think about encapsulation in this class.
         */
        public void Send(string recipiant, string message) {
			var userIdentifier = new UserIdentifier(recipiant);
			Message.PublishMessage(message, userIdentifier);
		}

        public List<PM> ReturnPMs()
        {
            List<IMessage> allPMs = new List<IMessage>();
            var latestMessagesReceived = Message.GetLatestMessagesReceived();
            latestMessagesReceived.ToString();
            allPMs.AddRange(latestMessagesReceived.ToList());
            

            //Gets Messages Sent
            var latestMessagesSent = Message.GetLatestMessagesSent();
            latestMessagesSent.ToString();
            allPMs.AddRange(latestMessagesSent.ToList());

            List<PM> PMs = new List<PM>();
            foreach (var message in allPMs)
            {
                PM p = new PM();
                p.Text = message.Text;
                PMs.Add(p);
             }

            return PMs;
        }
	}
}
