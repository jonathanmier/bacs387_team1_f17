﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterApp
{
	class Converter
	{
		public Joke Convert(JsonInput input)
		{
			Joke theJoke = JsonConvert.DeserializeObject<Joke>(input.Json);

			return theJoke;
		}
	}
}
