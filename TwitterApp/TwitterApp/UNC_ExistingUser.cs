﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;

namespace TwitterApp
{
    class UNC_ExistingUser: UNC_User
    {

        public string CheckPassword()
        {
            string check = "fail";

            List<string> userData = ReadFile();

            int filePosition = FindUser();

            string user = userData[filePosition];
            string pass = userData[filePosition+1];


            if (user == Username)
            {
                if (pass == Password)
                {
                    check = "Password Matches";
                }

            }

            return check;
        }

        public string GetTimeline()
        {

            var authenticatedUser = User.GetAuthenticatedUser();
            var homeTimeline = authenticatedUser.GetHomeTimeline();

            List<Tweetinvi.Models.ITweet> tweetinvi_Tweets = homeTimeline.ToList();

            List<string> unco_tweets = new List<string>();

            foreach(Tweetinvi.Models.ITweet tweet in tweetinvi_Tweets)
            {
                string creator = tweet.CreatedBy.ScreenName;
                string convertedTweet = tweet.ToString();
                unco_tweets.Add(creator + ":\t" + convertedTweet);
            }

            string timeline = "";

            foreach (string tweet in unco_tweets)
            {
                timeline = timeline + "\n" + tweet;
            }

            return timeline;

        }
    }
}
