﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;

namespace TwitterApp
{
    class UncTweet : UncMessage
    {
        public int Retweets { get; set; }
        public int Likes { get; set; }

        public UncTweet()
        {
            CharacterLimit = 140;
        }

        public void Send()
        {
            if (Text.Length <= CharacterLimit)
            {
                Tweet.PublishTweet(Text);
            }
        }

        

        public List<UncTweet> ReturnTweets()
        {
            RateLimit.RateLimitTrackerMode = RateLimitTrackerMode.TrackAndAwait;

            var userId = "Bacs387_Team1";
            var lastTweets = Timeline.GetUserTimeline(userId, 200).ToArray();

            var allTweets = new List<ITweet>(lastTweets);
            var beforeLast = allTweets;

            while (lastTweets.Length > 0 && allTweets.Count <= 3200)
            {
                var idOfOldestTweet = lastTweets.Select(x => x.Id).Min();
                //Console.WriteLine($"Oldest Tweet Id = {idOfOldestTweet}");

                var numberOfTweetsToRetrieve = allTweets.Count > 3000 ? 3200 - allTweets.Count : 200;
                var timelineRequestParameters = new UserTimelineParameters
                {
                    // MaxId ensures that we only get tweets that have been posted
                    // BEFORE the oldest tweet we received
                    MaxId = idOfOldestTweet - 1,
                    MaximumNumberOfTweetsToRetrieve = numberOfTweetsToRetrieve
                };

                lastTweets = Timeline.GetUserTimeline(userId, timelineRequestParameters).ToArray();
                allTweets.AddRange(lastTweets);
            }

            List<UncTweet> Tweets = new List<UncTweet>();
            foreach(var tweet in allTweets)
            {
                UncTweet t = new UncTweet();
                t.Text = tweet.Text;
                Tweets.Add(t);
            }

            return Tweets;
        }

        public string RetrieveTweets()
        {
            RateLimit.RateLimitTrackerMode = RateLimitTrackerMode.TrackAndAwait;

            /*
            RateLimit.QueryAwaitingForRateLimit += (sender, args) =>
            {
                Console.WriteLine($"Query : {args.Query} is awaiting for rate limits!");
            };
            */

            var userId = "Bacs387_Team1";
            var lastTweets = Timeline.GetUserTimeline(userId, 200).ToArray();

            var allTweets = new List<ITweet>(lastTweets);
            var beforeLast = allTweets;

            while (lastTweets.Length > 0 && allTweets.Count <= 3200)
            {
                var idOfOldestTweet = lastTweets.Select(x => x.Id).Min();
                //Console.WriteLine($"Oldest Tweet Id = {idOfOldestTweet}");

                var numberOfTweetsToRetrieve = allTweets.Count > 3000 ? 3200 - allTweets.Count : 200;
                var timelineRequestParameters = new UserTimelineParameters
                {
                    // MaxId ensures that we only get tweets that have been posted
                    // BEFORE the oldest tweet we received
                    MaxId = idOfOldestTweet - 1,
                    MaximumNumberOfTweetsToRetrieve = numberOfTweetsToRetrieve
                };

                lastTweets = Timeline.GetUserTimeline(userId, timelineRequestParameters).ToArray();
                allTweets.AddRange(lastTweets);


            }

            string test = "";

            foreach (ITweet a in allTweets)
            {
                test = test + "\n" + a.ToString();
            }

            return test;
        }
    }
}

