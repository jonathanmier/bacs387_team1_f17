﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterApp
{
    class UNC_User:IUser
    {
        public string Username { get; set; }
        public string Password { get; set; }


        public string FilePath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + "\\Users.csv";

        public enum decision
        {
            NoInfoPresent,
            UserDontExist,
            WrongPassword,
            GoodInfo
        }

        public decision DoesUserExist()
        {

            decision d;

            DoesUserFileExist();

            int answer = FindUser();


            if (Username == "" || Password == "")
            {
                d = decision.NoInfoPresent;

            }
            else if (answer == -1)
            {
                //SD: -1 means no user was found

                d = decision.UserDontExist;
            }
            else
            {
                UNC_ExistingUser unc_eu = new UNC_ExistingUser();
                unc_eu.Username = Username;
                unc_eu.Password = Password;
                string response = unc_eu.CheckPassword();

                if (response == "Password Matches")
                {
                    unc_eu.Username = Username;
                    unc_eu.Password = Password;
                    d = decision.GoodInfo;
                }
                else
                {
                    d = decision.WrongPassword;
                }
            }

            return d;
        }

        private void DoesUserFileExist()
        {
            bool response = File.Exists(FilePath);

            if (response == false)
            {
                CreateUserFile();
                WriteNewUserHeader();
            }
        }

        private void CreateUserFile()
        {
            try
            {
                File.Create(FilePath).Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine("Creating the file failed");
                Console.WriteLine(e);
                Console.Read();
            }
        }

        private void WriteNewUserHeader()
        {
            using (StreamWriter sw = new StreamWriter(FilePath))
            {
                
                sw.WriteLine("UserName,Password");
                sw.Dispose();
            }
        }

        public List<string> ReadFile()
        {
            List<string> userFile = new List<string>();

            using (StreamReader sr = new StreamReader(FilePath))
            {
                string all = sr.ReadToEnd().Replace(Environment.NewLine, ",");
                userFile = all.Split(',').ToList();
                sr.Dispose();
            }

            return userFile;
        }

        public int FindUser()
        {
            List<string> userFile = ReadFile();
            string test = "";
            foreach(string s in userFile)
            {
                test = s +","+ test;
            }
            

            int selectionValue = -1;

            for (int i = 0; i < userFile.Count; i = i + 2)
            {
                string name = userFile[i];
                if (userFile[i] == Username)
                {
                    selectionValue = i;
                }

            }

            return selectionValue;
        }
    }
}
