﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterApp
{
    class UNC_NewUser: UNC_User
    {
        public void WriteNewUserIntoFile()
        {
            using (StreamWriter sw = new StreamWriter(FilePath, append: true))
            {
                string line = String.Format("{0},{1}", Username, Password);
                sw.WriteLine(line);
                sw.Dispose();
            }
        }
    }
}
