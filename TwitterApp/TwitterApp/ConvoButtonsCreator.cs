﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;

namespace TwitterApp
{
	class ConvoButtonsCreator
	{
		public List<Tweetinvi.Models.IMessage> recieved { get; set; }
		public List<Tweetinvi.Models.IMessage> sent { get; set; }


		public List<string> filterConvos()
		{
			var persons = new List<string>();

			foreach (Tweetinvi.Models.IMessage c in recieved)
			{
				if (!persons.Contains(c.SenderScreenName))
				{
					persons.Add(c.SenderScreenName);
				}
			}
			foreach (Tweetinvi.Models.IMessage c in sent)
			{
				if (!persons.Contains(c.RecipientScreenName))
				{
					persons.Add(c.RecipientScreenName);
				}

			}

			return persons;
		}
	}
}
