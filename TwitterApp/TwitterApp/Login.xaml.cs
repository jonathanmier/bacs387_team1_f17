﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TwitterApp
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        //private static int count = 0;

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {

            //SD: This must be done everytime to make sure the keys are set for the application account
            TwittApp app = new TwittApp();
            app.DefaultKeys();

            UNC_User user = new UNC_User();
            user.Username = UserNameTextBox.Text;
            user.Password = UNCSecurity.SHA512(PasswordBox.Password);

            //SD: Looking up if a user exists, and getting a response
            UNC_User.decision choice = user.DoesUserExist();

            if (choice == UNC_User.decision.NoInfoPresent)
            {
                MessageBox.Show("All information must be entered before proceeding. \nPlease try again.");
                return;
            }
            else if (choice == UNC_User.decision.UserDontExist)
            {
                UserNameTextBox.Text = "";
                PasswordBox.Password = "";

                Sign_Up signup = new Sign_Up();
                signup.Show();
                this.Close();
            }
            else if (choice == UNC_User.decision.GoodInfo)
            {
                Home home = new Home();
                home.uname = user.Username;
                home.Show();

                this.Close();
            }
            else if (choice == UNC_User.decision.WrongPassword)
            {
                MessageBox.Show("The password you have entered does not match." +
                    "\nLogin Failed." + "\nYou are free to try again.");
                UserNameTextBox.Text = "";
                PasswordBox.Password = "";
            }
            else
            {
                MessageBox.Show("Incorrect information, please try again. If you have yet to sign up, please return to the previous screen and sign up.");
            }

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

    }
}
