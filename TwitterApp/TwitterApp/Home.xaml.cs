﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tweetinvi;
using Tweetinvi.Models;
using System.IO;

namespace TwitterApp
{
	/// <summary>
	/// Interaction logic for Home.xaml
	/// </summary>

	
    public partial class Home : Window
    {
        Conversation Convo = new Conversation();
        public string uname { get; set; }

        public Home()
        {
            InitializeComponent();

			create_buttons();

			Loaded += Home_Loaded;
            

        }

        private void Home_Loaded(object sender, RoutedEventArgs e)
        {
            UncTweet tweet = new UncTweet();
            UNC_ExistingUser userLogin = new UNC_ExistingUser();
            userLogin.Username = uname;

			JokeRetriever retriever = new JokeRetriever();
			JsonInput input = new JsonInput();
			input.Json = retriever.retrieve();
			Converter converterObject = new Converter();
			Joke convertedJoke = converterObject.Convert(input);

			tweet.Text = userLogin.Username + ": " + convertedJoke.Value.joke;
			
			tweet.Send();
            
            RetrieveTweetsTweetBox.Text = tweet.RetrieveTweets();
        }




        private void Button_Click(object sender, RoutedEventArgs e)
        {
            UncTweet tweet = new UncTweet();
			UNC_ExistingUser verified_user = new UNC_ExistingUser();

            verified_user.Username = uname;

            tweet.Text = verified_user.Username +": "+ txt_tweet.Text;
            tweet.Send();
            //SD: Clear the textbox, that way we can see that a tweet has been sent
            txt_tweet.Text = "";

        }

        private void GetTimeLineButton_Click(object sender, RoutedEventArgs e)
        {
			UNC_ExistingUser eu = new UNC_ExistingUser();
            TimeLineTextBox.Text = eu.GetTimeline();            
        }

		private void create_buttons()
		{
			//Gets Messages Recieved
			var latestMessagesReceived = Message.GetLatestMessagesReceived();
			latestMessagesReceived.ToString();

			//Gets Messages Sent
			var latestMessagesSent = Message.GetLatestMessagesSent();
			latestMessagesSent.ToString();

			//Filters through messages to retrieve convos
			ConvoButtonsCreator creator = new ConvoButtonsCreator();
			creator.recieved = latestMessagesReceived.ToList();
			creator.sent = latestMessagesSent.ToList();

			//Ceates buttons 
			foreach (string person in creator.filterConvos())
			{
				System.Windows.Controls.Button newBtn = new Button();
				newBtn.Content = person;
				newBtn.Name = person;
				newBtn.Click += new RoutedEventHandler(newBtn_Click);
				sp.Children.Add(newBtn);
			}
		}

		private void btn_message_Click(object sender, RoutedEventArgs e)
		{

            UNC_ExistingUser verified_user = new UNC_ExistingUser();
            verified_user.Username = uname;
			if (usrn_name_txt.Text.Equals(""))
			{
				Convo.Send(Convo.Participants.Recipiant, verified_user.Username + ": " + txt_message.Text);
				txt_message.Text = "";
			}
			else
			{
				PM pm = new PM();
				pm.Send(usrn_name_txt.Text, txt_message.Text);
				usrn_name_txt.Text = "";
				sp.Children.Clear();
				create_buttons();

			}
            
		}
		private void newBtn_Click(object sender, RoutedEventArgs e)
		{
			lst_convo.Items.Clear();
			usrn_name_txt.Text = "";

			//Retrieves messages Recieved
			var latestMessagesReceived = Message.GetLatestMessagesReceived();
			latestMessagesReceived.ToString();

			//Retrieved messages Sent
			var latestMessagesSent = Message.GetLatestMessagesSent();
			latestMessagesSent.ToString();

			//Gets conversation button object
			Button btn = sender as Button;
			string user = btn.Name.ToString();
			lbl_convo1.Content = user;

			//Participants
			Participants participants = new Participants();
			participants.Recipiant = user;
			participants.Sender = "BACS485_Team1";

			//Conversation
			Conversation conversation = new Conversation();
			conversation.Participants = participants;
			conversation.MessagesRecieved = latestMessagesReceived.ToList();
			conversation.MessagesSent = latestMessagesSent.ToList();

            Convo = conversation;
            
			//Displays Messages 
			List<IMessage> sorted = conversation.sortMessages();
			foreach (IMessage message in sorted.OrderBy(d => d.CreatedAt))
			{
				if (message.SenderScreenName.Equals(conversation.Participants.Sender))
				{
					lst_convo.Items.Add("(" + message.RecipientScreenName + "): " + message.Text);
				}
				else
				{
					lst_convo.Items.Add("(" + message.SenderScreenName + "): " + message.Text);
				}
			}

            lst_convo.SelectedIndex = sorted.Count - 1;
            lst_convo.ScrollIntoView(lst_convo.Items[lst_convo.Items.Count - 1]);

            //MessageBox.Show(x);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            UNCTweetFile tweetFile = new UNCTweetFile();
            tweetFile.SaveTweetsToFile();
        }

        private void DisplayMessagesBtn_Click(object sender, RoutedEventArgs e)
        {
            UncMessage message = new UncMessage();
            PM pm = new PM();
            UncTweet tweets = new UncTweet();
            message.allMessages.AddRange(pm.ReturnPMs());
            message.allMessages.AddRange(tweets.ReturnTweets());
            
            foreach(UncMessage mes in message.allMessages)
            {
				AllMessagesTextBx.Text += mes.Text + "\n"; 
            }
        }

        private void btnSaveMessages_Click(object sender, RoutedEventArgs e)
        {
           //Saves messages to a text file
            Convo.SaveMessages();
        }
    }
}
