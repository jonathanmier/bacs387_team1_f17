﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TwitterApp
{
    /// <summary>
    /// Interaction logic for Sign_Up.xaml
    /// </summary>
    public partial class Sign_Up : Window
    {
        public Sign_Up()
        {
            InitializeComponent();
        }

        private void SignUpButton_Click(object sender, RoutedEventArgs e)
        {
            UNC_NewUser NewUser = new UNC_NewUser();
            NewUser.Username = UserNameTextBox.Text;
            NewUser.Password = UNCSecurity.SHA512(PasswordBox.Password);

            UNC_User.decision decision = NewUser.DoesUserExist();
            
            if (decision != UNC_User.decision.UserDontExist)
            {
                MessageBox.Show("The username you have entered already exists, please choose another one. \nThank you!");
                UserNameTextBox.Text = "";
                PasswordBox.Password = "";
            }
            else
            {
                NewUser.WriteNewUserIntoFile();

                MessageBox.Show("Awesome you are signed up! You will be taken to the " +
                    "login screen to verify that you are in our system. \nThank you!");

                Login login = new Login();
                login.Show();

                this.Close();
            }

            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }
    }
}
