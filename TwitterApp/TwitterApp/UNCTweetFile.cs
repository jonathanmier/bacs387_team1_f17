﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;

namespace TwitterApp
{
    class UNCTweetFile
    {
        /*
         * MS: This file saving behavior feels like
         * it belongs in a class of its own.  A TweetFile
         * class that you can hand a Tweet to and say "SaveTweet"
         * 
         * This class has fairly low cohesion as it is... it 
         * handles several very distinct sets of behavior rather than 
         * having a single responsibility. 
         * 
         * Google "Uncle Bob single responsibility principle" for more on this idea.
         */
        public void SaveTweetsToFile()
        {
            string path = CreateFilePath();
            DoesTweetFileExist(path);
            string line = RetrieveTweets();
            WriteTweetFile(path, line);
        }

        public static string CreateFilePath()
        {
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string path = desktopPath + "\\tweets.txt";

            return path;
        }
        public static void DoesTweetFileExist(string path)
        {
            bool t = File.Exists(path);

            if (t == false)
            {
                CreateTweetFile(path);
            }

        }

        public static void CreateTweetFile(string path)
        {
            try
            {
                File.Create(path).Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine("Creating the file failed");
                Console.WriteLine(e);
                Console.Read();
            }
        }

        public static void WriteTweetFile(string path, string line)
        {
            List<string> abc = line.Split('\n').ToList();

            using (StreamWriter sw = new StreamWriter(path, append: true))
            {
                foreach (string a in abc)
                {
                    sw.WriteLine(a);
                }

                sw.Dispose();
            }
        }

        public string RetrieveTweets()
        {
            RateLimit.RateLimitTrackerMode = RateLimitTrackerMode.TrackAndAwait;

            /*
            RateLimit.QueryAwaitingForRateLimit += (sender, args) =>
            {
                Console.WriteLine($"Query : {args.Query} is awaiting for rate limits!");
            };
            */

            var userId = "Bacs387_Team1";
            var lastTweets = Timeline.GetUserTimeline(userId, 200).ToArray();

            var allTweets = new List<ITweet>(lastTweets);
            var beforeLast = allTweets;

            while (lastTweets.Length > 0 && allTweets.Count <= 3200)
            {
                var idOfOldestTweet = lastTweets.Select(x => x.Id).Min();
                //Console.WriteLine($"Oldest Tweet Id = {idOfOldestTweet}");

                var numberOfTweetsToRetrieve = allTweets.Count > 3000 ? 3200 - allTweets.Count : 200;
                var timelineRequestParameters = new UserTimelineParameters
                {
                    // MaxId ensures that we only get tweets that have been posted
                    // BEFORE the oldest tweet we received
                    MaxId = idOfOldestTweet - 1,
                    MaximumNumberOfTweetsToRetrieve = numberOfTweetsToRetrieve
                };

                lastTweets = Timeline.GetUserTimeline(userId, timelineRequestParameters).ToArray();
                allTweets.AddRange(lastTweets);


            }

            string test = "";

            foreach (ITweet a in allTweets)
            {
                test = test + "\n" + a.ToString();
            }

            return test;
        }
    }
}
